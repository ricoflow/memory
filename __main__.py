import sys
from PyQt6 import QtWidgets as qtw
from PyQt6 import QtGui as qtg
from PyQt6 import QtCore as qtc
from tiles import TILES
from random import randint
from itertools import cycle

# CONSTANTS
DEBUG = False
TILE_FLIP_WAIT = 500  # milliseconds
TILE_AREA_BORDER_WIDTH = 50
TILE_BETWEEN_SPACE = 20
TILES_WIDE = 9
TILES_TALL = 8
TOTAL_TILES = TILES_WIDE * TILES_TALL
TILE_DESIGN_SIZE = 8
TILE_FINAL_SIZE = 96
SCORE_WIDTH = 200
WINDOW_BORDER = 20
SCORE_HEIGHT = 200
DUP_PLACEMENTS = TOTAL_TILES // len(TILES)
SCREEN_WIDTH = TILE_FINAL_SIZE * TILES_WIDE + (TILE_BETWEEN_SPACE * (TILES_WIDE - 1)) + (SCORE_WIDTH * 2) \
               + (TILE_AREA_BORDER_WIDTH * 2)
SCREEN_HEIGHT = TILE_FINAL_SIZE * TILES_TALL + (TILE_BETWEEN_SPACE * (TILES_TALL - 1))
SCORE_CORNER_PLACEMENTS = [
    # top_left (player1)
    [0, 0, SCORE_WIDTH, SCORE_HEIGHT],
    # top_right (player2)
    [0, 0, SCORE_WIDTH, SCORE_HEIGHT],
    # bottom_right (player3)
    [0, SCREEN_HEIGHT - SCORE_HEIGHT, SCORE_WIDTH, SCORE_HEIGHT],
    # bottom_left (player4)
    [0, SCREEN_HEIGHT - SCORE_HEIGHT, SCORE_WIDTH, SCORE_HEIGHT],
]
BACK_PATTERN = b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF'
BACK_COLOR = 'pink'

PLAYER_NAME_MAX_LENGTH = 20

# Certain requirements must be met, or we could end up mismatched tiles
assert (DUP_PLACEMENTS % 2 == 0 and TOTAL_TILES % len(TILES) == 0)


class TileIterator(object):
    """
    Provides tiles for board placement from a collection of tiles.
    """

    def __init__(self, tiles: list = TILES, dup_placements: int = 4) -> None:
        """
        Initializes the tile iterator.

        :param tiles: A collection of tiles (list of bytes)
        :param dup_placements: The number of times a tile from the collection can be placed on the board.
        """
        # Don't alter the original list
        self.tiles = tiles.copy()
        # Tracker for tiles that have been used up
        self.tracker = dict()
        # Allowed number of duplicate tile placements
        self.dup_placements = dup_placements

    def __iter__(self):
        return self

    def __next__(self):
        """
        Provides the next tile from the collection. Raises a StopIteration exception when there are no more tiles.
        """
        if not self.tiles:
            raise StopIteration

        i = randint(0, len(self.tiles) - 1)
        try:
            tile, color = self.tiles[i]
            # Track a new tile or increment the count for a previously tracked tile
            self.tracker[tile] = self.tracker.get(tile, 0) + 1

            # We have reached the max placements for this tile, so delete it from the collection
            if self.tracker[tile] >= self.dup_placements:
                del self.tiles[i]
            return tile, color
        except IndexError:
            raise StopIteration


class NumPlayersDialog(qtw.QDialog):
    """
    A dialog box for selecting the number of players (1-4).
    """

    players = qtc.pyqtSignal(list)

    def __init__(self, parent: qtw.QWidget = None) -> None:
        super().__init__(parent, modal=True)

        self.setWindowTitle('How many players?')
        self.setWindowFlag(qtc.Qt.WindowType.WindowCloseButtonHint, False)

        layout = qtw.QFormLayout()
        label = qtw.QLabel('How many players (1-4)?')
        self.num_players = qtw.QSpinBox(minimum=1, maximum=4)
        self.setLayout(layout)

        self.layout().addRow(label, self.num_players)

        self.layout().addRow(qtw.QPushButton('Start Game', clicked=self.accept))

        self.player_names = []

    def accept(self) -> None:
        """
        Accepts the number of players and instantiates one name-entry dialog box for each player.
        """
        num_players = self.num_players.value()

        for p_num in range(1, num_players + 1):
            player_entry_dialog = PlayerNameDialog(p_num)
            player_entry_dialog.name.connect(self.add_player)
            player_entry_dialog.exec()

        self.players.emit(self.player_names)
        super().accept()

    def reject(self):
        """
        Prevents ESC key from closing the dialog.
        """
        pass

    def add_player(self, name):
        self.player_names.append(name)


class PlayerNameDialog(qtw.QDialog):
    """
    A dialog box for a player to enter their name.
    """
    name = qtc.pyqtSignal(str)

    def __init__(self, player_num: int, parent: qtw.QWidget = None) -> None:
        super().__init__(parent, modal=True)

        self.setWindowTitle(f'Player {player_num}')
        # Disable 'close' button on the dialog box
        self.setWindowFlag(qtc.Qt.WindowType.WindowCloseButtonHint, False)

        label = qtw.QLabel(f'Player {player_num}, please enter you name:')
        self.name_textbox = qtw.QLineEdit()
        self.name_textbox.setMaxLength(PLAYER_NAME_MAX_LENGTH)
        submit = qtw.QPushButton('Done', clicked=self.accept)
        self.setLayout(qtw.QFormLayout())
        self.layout().addRow(label, self.name_textbox)
        self.layout().addRow(submit)

    def accept(self):
        """
        Emits the player's name and calls the accept method of the base parent class.
        """
        self.name.emit(self.name_textbox.text())
        self.close()
        super().accept()

    def reject(self):
        """
        Prevents ESC key from closing the dialog.
        """
        pass


class PlayAgainDialog(qtw.QDialog):
    """
    A dialog box asking if the user wants to play again.
    """

    same_players_sig = qtc.pyqtSignal(bool)

    def __init__(self, parent: qtw.QWidget = None) -> None:
        super().__init__(parent, modal=True)

        self.setWindowTitle('Play Again?')
        # Disable 'close' button on the dialog box
        self.setWindowFlag(qtc.Qt.WindowType.WindowCloseButtonHint, False)

        label = qtw.QLabel('Play Again?')
        yes = qtw.QPushButton('Play Again', clicked=self.accept)
        no = qtw.QPushButton('Quit', clicked=self.reject)
        self.same_players_check = qtw.QCheckBox('Use Same Players')

        self.setLayout(qtw.QFormLayout())
        self.layout().addRow(label)
        self.layout().addRow(self.same_players_check)
        self.layout().addRow(yes, no)

    def accept(self):
        """
        Accepts the Play Again button-push and emits a boolean indicating if the player names should be re-used from
        the last game.
        """
        self.same_players_sig.emit(self.same_players_check.isChecked())
        self.close()
        super().accept()

    def reject(self):
        """
        Prevents ESC key from closing the dialog.
        """
        #TODO - App should exit here
        pass
        #qtc.QCoreApplication.quit()


class Tile(qtw.QGraphicsObject):
    """
    A Tile object. Includes a 'face' and a 'back' and a method for toggling between the two. Handles user mouse clicks.
    """

    click_sig = qtc.pyqtSignal(qtw.QGraphicsObject)
    block_sig = qtc.pyqtSignal(bool)

    def __init__(self, color: str, left: int, top: int, pattern: bytes, **kwargs) -> None:
        """
        Initializes the Tile.

        :param color: A color string.
        :param left: The left (x) position of the top-left corner of the tile.
        :param top: The top (y) position of the top-left corner of the tile.
        :param pattern: A string of bytes (one byte per each row of the tile).
        :return: None.
        """
        super().__init__(**kwargs)

        self.color = color
        self.pattern = pattern

        self.back_bm, self.back_pen = self.generate_bitmap(face=False)
        self.face_bm, self.face_pen = self.generate_bitmap(face=True)

        self.setPos(left, top)

        # Show faces for debugging
        if DEBUG:
            self.bitmap = self.face_bm
            self.pen = self.face_pen
        else:
            self.bitmap = self.back_bm
            self.pen = self.back_pen

        self.face = False

    def mousePressEvent(self, event: qtw.QGraphicsSceneMouseEvent) -> None:
        """
        Handles mouse click events. Temporarily disables Tile clickability so the graphics loop will have a chance to
        update the face of the tile.
        :param event: qtw.QGraphicsSceneMouseEvent.
        :return: None.
        """
        if not self.face:
            self.flip()
            # block tiles from being clicked momentarily to allow graphics to update via event loop
            self.block_sig.emit(True)
            qtc.QTimer.singleShot(TILE_FLIP_WAIT, self.send_signal)

    def send_signal(self) -> None:
        """
        Emits a mouse click signal and re-enables tile clickability.
        :return: None
        """
        # send the click
        self.click_sig.emit(self)
        # stop blocking clicks
        self.block_sig.emit(False)

    def boundingRect(self):
        """
        Specifies the Tile's bounding rectangle.
        """
        return qtc.QRectF(0, 0, self.bitmap.width(), self.bitmap.height())

    def paint(self, painter: qtg.QPainter, option: qtw.QStyleOptionGraphicsItem, widget: qtw.QWidget) -> None:
        """
        Paints the Tile object using the bitmap and pen instance variables.

        :param painter: qtg.QPainter.
        :param option: qtw.QStyleOptionGraphicsItem.
        :param widget: qtw.QWidget.
        :return: None.
        """
        painter.setPen(self.pen)
        painter.drawPixmap(0, 0, self.bitmap)

    def generate_bitmap(self, face: bool) -> (qtg.QBitmap, qtg.QPen):
        """
        Generates a bitmap of a Tile object and scales it to the appropriate size.

        :param face: A boolean indicating whether to draw the face or the back of the tile.
        :return: A tuple containing the bitmap and the pen to draw it with.
        """
        if face:
            bitmap = qtg.QBitmap.fromData(
                qtc.QSize(TILE_DESIGN_SIZE, TILE_DESIGN_SIZE),
                self.pattern
            )
            pen = qtg.QPen(qtg.QColor(self.color))
        else:
            bitmap = qtg.QBitmap.fromData(
                qtc.QSize(TILE_DESIGN_SIZE, TILE_DESIGN_SIZE),
                BACK_PATTERN
            )
            pen = qtg.QPen(qtg.QColor(BACK_COLOR))

        # Enlarge the bitmaps
        transform = qtg.QTransform()
        scale_factor = TILE_FINAL_SIZE / TILE_DESIGN_SIZE
        transform.scale(scale_factor, scale_factor)
        bitmap = bitmap.transformed(transform)

        return bitmap, pen

    def flip(self) -> None:
        """
        'Flips' a tile. Toggles the bitmap between the face and back of the Tile and schedules a repaint.
        :return: None
        """
        self.face = not self.face

        if self.face:
            self.bitmap, self.pen = self.face_bm, self.face_pen
        else:
            self.bitmap, self.pen = self.back_bm, self.back_pen

        # Places objects to be redrawn into the event queue
        self.update()


class ScoreCorner(qtw.QGraphicsObject):
    """
    A player's scoreboard to keep track of their matches.
    """
    chosen = qtc.pyqtSignal(int)

    def __init__(self, left: int, top: int, width: int, height: int, player_index: int, **kwargs) -> None:
        super().__init__(**kwargs)

        self.player_index = player_index
        self.score = 0

        # Different colored brushes
        self.default_brush = qtg.QBrush(qtg.QColor('pink'))
        self.selected_brush = qtg.QBrush(qtg.QColor('yellow'))
        player_font = qtg.QFont('Impact', 20)
        # Score may have its own font later
        score_font = qtg.QFont('Impact', 80)

        self.score_rect = qtw.QGraphicsRectItem(parent=self)
        self.score_rect.setRect(0, 0, width, height)
        # setPos refers to the parent coordinate system
        self.setPos(left, top)
        # start with the default brush
        self.score_rect.setBrush(self.default_brush)

        # Player name
        self.name_text = qtw.QGraphicsSimpleTextItem('', parent=self.score_rect)
        self.name_text.setFont(player_font)

        self.score_text = qtw.QGraphicsSimpleTextItem('', parent=self.score_rect)
        self.score_text.setFont(score_font)

    def mousePressEvent(self, event: qtw.QGraphicsSceneMouseEvent) -> None:
        """
        Toggles the color of the chosen player and emits the player index number.

        :param event: qtw.QGraphicsSceneMouseEvent.
        :return: None.
        """
        self.toggle_color()
        self.chosen.emit(self.player_index)

    def set_name(self, name: str) -> None:
        """
        Sets the Name of the player in the QGraphicsSimpleTextItem object and adjusts the size and alignment of the
        text object.

        :param name: Player Name string.
        :return: None.
        """
        self.name_text.setText(name)
        # relative-to-parent text placement
        name_width = self.name_text.boundingRect().width()
        self.name_text.setPos((self.boundingRect().width() - name_width) / 2, 10)

    def update_score(self, score: int = 0) -> None:
        """
        Increments the player's score by 1 and adjusts the alignment of the score QGraphicsSimpleTextItem.

        :param score: Integer value to add to the score.
        :return: None
        """
        self.score = self.score + score

        self.score_text.setText(str(self.score))

        # relative-to-parent score placement
        score_width = self.score_text.boundingRect().width()
        score_height = self.score_text.boundingRect().height()
        self.score_text.setPos((self.boundingRect().width() - score_width) / 2,
                               (self.boundingRect().height() - score_height) / 2)

        self.update()

    def toggle_color(self) -> None:
        """
        Toggles the color of the player's ScoreCorner between active and waiting-for-turn.
        :return: None
        """
        if self.score_rect.brush() == self.default_brush:
            self.score_rect.setBrush(self.selected_brush)
        else:
            self.score_rect.setBrush(self.default_brush)

        self.update(self.boundingRect())

    def boundingRect(self) -> qtc.QRectF:
        return self.score_rect.boundingRect()

    def paint(self, painter: qtg.QPainter, option: qtw.QStyleOptionGraphicsItem, widget: qtw.QWidget) -> None:
        """
        Paints the ScoreCorner object.

        :param painter: qtg.QPainter.
        :param option: qtw.QStyleOptionGraphicsItem.
        :param widget: qtw.QWidget.
        :return: None.
        """
        painter.drawRect(self.boundingRect())


class HidingRect(qtw.QGraphicsRectItem):
    """
    An invisible rectangle to be placed over areas where we don't want to allow the user to click.
    """

    def __init__(self, left: int, top: int, width: int, height: int) -> None:
        super().__init__(qtc.QRectF(0, 0, width, height))
        self.setPos(left, top)
        # trigger for mouseEvent capture signals
        self.hiding = True

    def mousePressEvent(self, event: qtw.QGraphicsSceneMouseEvent) -> None:
        """
        Stops mouse-clicks from propagating when the hiding rectangle is enabled.
        :param event: qtw.QGraphicsSceneMouseEvent.
        :return: None
        """
        if self.hiding:
            event.accept()
        else:
            event.ignore()


class Scene(qtw.QGraphicsScene):
    """
    The scene where the graphics will be placed.
    """

    game_over_sig = qtc.pyqtSignal()

    def __init__(self, player_names: list) -> None:
        """
        Initializes the game board.
        :param player_names: A list of player names.
        :return: None.
        """
        super().__init__()
        self.setBackgroundBrush(qtg.QBrush(qtg.QColor('black')))

        self.setSceneRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

        self.players = []

        self.centre_message = None

        self.tiles_removed = 0

        # Starting position for tiles
        x_start = 0 + SCORE_WIDTH + TILE_AREA_BORDER_WIDTH
        x = x_start
        y = 0
        placed_count = 0

        # Get a tile iterator to lay the tiles
        tile_iter = TileIterator(TILES, dup_placements=DUP_PLACEMENTS)

        # Place an invisible rectangle over the scoreboards
        self.left_score_rect = HidingRect(0, 0, SCORE_WIDTH, SCREEN_HEIGHT)
        self.right_score_rect = HidingRect(SCREEN_WIDTH - SCORE_WIDTH, 0, SCORE_WIDTH, SCREEN_HEIGHT)
        self.tile_area_rect = HidingRect(x_start,
                                         0,
                                         SCREEN_WIDTH - (2 * (SCORE_WIDTH + TILE_AREA_BORDER_WIDTH)),
                                         SCREEN_HEIGHT)

        # Add the scoreboard corners
        for i, p_name in enumerate(player_names):
            if i in (0, 3):
                corner = ScoreCorner(*SCORE_CORNER_PLACEMENTS[i], i)
                corner.setParentItem(self.left_score_rect)
            else:
                corner = ScoreCorner(*SCORE_CORNER_PLACEMENTS[i], i)
                corner.setParentItem(self.right_score_rect)

            # Hiding needs the child tiles to stack behind parent hiding rectangle
            corner.setFlag(qtw.QGraphicsObject.GraphicsItemFlag.ItemStacksBehindParent)

            corner.set_name(p_name)
            corner.update_score(0)

            # connect the score corner to starting player selection function
            corner.chosen.connect(self.accept_starting_player)

            self.players.append(corner)

        # Create a player cycler
        self.player_cycle = cycle(self.players)

        # We will choose the current player later
        self.current_player = None

        # Keep track of flipped tiles
        self.flipped_tiles = []

        # Place the tiles in a grid
        for i in range(TOTAL_TILES):
            try:
                if placed_count > 0 and placed_count % TILES_WIDE == 0:
                    # start a new row
                    y += TILE_FINAL_SIZE + TILE_BETWEEN_SPACE
                    x = x_start
                tile_bytes, color = next(tile_iter)
                tile_obj = Tile(color, x, y, tile_bytes, parent=self.tile_area_rect)

                # Hiding needs the child tiles to stack behind parent hiding rectangle
                tile_obj.setFlag(qtw.QGraphicsObject.GraphicsItemFlag.ItemStacksBehindParent)

                self.addItem(tile_obj)
                placed_count += 1
                x += TILE_FINAL_SIZE + TILE_BETWEEN_SPACE
                # connect the tile
                tile_obj.click_sig.connect(self.match_detection)
                tile_obj.block_sig.connect(self.toggle_tile_clickability)
            except StopIteration:
                break

        # Add the invisible rectangles
        self.addItem(self.left_score_rect)
        self.addItem(self.right_score_rect)
        self.addItem(self.tile_area_rect)

        # Disable clicking of the tiles until a player is chosen
        self.disable_tiles()
        self.disable_scoreboard()
        self.pick_starting_player()

    def toggle_tile_clickability(self, state: bool) -> None:
        """
        A slot to receive a signal to toggle mouse-click blocking in the tile area.
        :param state: A boolean indicating if mouse-click blocking should be enabled or disabled.
        :return: None.
        """
        self.tile_area_rect.hiding = state

    def disable_tiles(self) -> None:
        """
        Disables mouse-clicking in the tile area.
        :return: None.
        """
        self.tile_area_rect.hiding = True

    def enable_tiles(self) -> None:
        """
        Enables mouse-clicking in the tile area.
        :return: None.
        """
        self.tile_area_rect.hiding = False

    def disable_scoreboard(self) -> None:
        """
        Disables mouse-clicking in the scoreboard areas.
        :return: None
        """
        self.right_score_rect.hiding = True
        self.left_score_rect.hiding = True

    def enable_scoreboard(self) -> None:
        """
        Enables mouse-clicking in the scoreboard areas.
        :return: None.
        """
        self.right_score_rect.hiding = False
        self.left_score_rect.hiding = False

    def pick_starting_player(self) -> None:
        """
        Generates a message for the user to pick the starting player and enables mouse-clicking in the score board
        areas.
        :return: None.
        """
        font = qtg.QFont('Impact', 80)
        self.centre_message = qtw.QGraphicsSimpleTextItem('Click the player who goes first', parent=self.tile_area_rect)
        self.centre_message.setBrush(qtg.QBrush(qtg.QColor('white')))
        self.centre_message.setPen(qtg.QPen())
        self.centre_message.setFont(font)
        msg_width = self.centre_message.boundingRect().width()
        msg_height = self.centre_message.boundingRect().height()
        self.centre_message.setPos(
            (self.tile_area_rect.boundingRect().width() - msg_width) / 2,
            (SCREEN_HEIGHT - msg_height) / 2
        )
        self.addItem(self.centre_message)
        # enable the scoreboards to be click-able
        self.enable_scoreboard()

    def accept_starting_player(self, player_index: int) -> None:
        """
        A slot to receive a signal when a player name is clicked. The starting player is set, and mouse-clicks are
        disabled in the score board area.

        :param: The player index number recieved from the signal.
        :return: None.
        """
        # After a player is chosen, disable the scoreboards from being clicked
        self.disable_scoreboard()
        # Remove the 'choose a player' message
        self.centre_message.setText('')

        # set the current player the user chose and get the cycler to the right position
        target_player = self.players[player_index]
        current_player = next(self.player_cycle)
        while current_player != target_player:
            current_player = next(self.player_cycle)
        else:
            self.current_player = current_player

        # start the game. release the tiles
        self.enable_tiles()

    def next_player(self) -> None:
        """
        Moves the current active player to the next active player and toggles the color of both players.
        :return: None.
        """
        self.current_player.toggle_color()
        self.current_player = next(self.player_cycle)
        self.current_player.toggle_color()
        self.enable_tiles()

    def match_detection(self, tile: Tile) -> None:
        """
        A slot that is triggered whenever a tile is clicked. Responsible for detecting matches, incrementing player
        scores, and moving play to the next player.

        :param tile: The Tile object that has been clicked.
        :return: None
        """
        if len(self.flipped_tiles) == 0:
            # first tile to be flipped.
            self.flipped_tiles.append(tile)

        elif len(self.flipped_tiles) == 1:
            # disable clicking
            self.disable_tiles()
            self.flipped_tiles.append(tile)

            if self.flipped_tiles[0].pattern == self.flipped_tiles[1].pattern:
                # match
                self.current_player.update_score(1)
                for tile in self.flipped_tiles:
                    self.removeItem(tile)
                    self.tiles_removed += 1
                self.flipped_tiles = []
                # allow clicking again
                self.enable_tiles()
            else:
                # no match. flip the tiles back over
                for tile in self.flipped_tiles:
                    tile.flip()
                self.flipped_tiles = []
                # change players
                self.next_player()

        if self.tiles_removed == TOTAL_TILES:
            # call endgame function
            self.game_over()

    def game_over(self) -> None:
        """
        This function is called when all of the tiles on the board have been matched. Responsible for assigning a
        winner, displaying a message indicating the winner, and emitting a signal to initiate a 'Play Again' dialog.

        :return: None.
        """
        # determine the winning player
        # TODO: Handle Ties
        winner = max(self.players, key=lambda x: int(x.score_text.text()))
        self.centre_message.setText(f'{winner.name_text.text()} WINS!!!')
        msg_width = self.centre_message.boundingRect().width()
        msg_height = self.centre_message.boundingRect().height()
        self.centre_message.setPos(
            (self.tile_area_rect.boundingRect().width() - msg_width) / 2,
            (SCREEN_HEIGHT - msg_height) / 2
        )
        # TODO: the play again box should not cover up the Winner Message
        self.game_over_sig.emit()


class MainWindow(qtw.QMainWindow):
    """
    The main window of the application.
    """
    def __init__(self):
        super().__init__()
        # Main UI code goes here
        self.setWindowTitle('Memory Game')
        self.players = []
        self.scene = None

        self.setFixedSize(qtc.QSize(SCREEN_WIDTH + WINDOW_BORDER, SCREEN_HEIGHT + WINDOW_BORDER))

        self.start_fresh_game(same_players=False)

    def game_over(self) -> None:
        """
        A slot to receive a game over signal. Responsible for opening a PlayAgainDialog.

        :return: None.
        """
        new_game_dialog = PlayAgainDialog()
        new_game_dialog.same_players_sig.connect(self.start_fresh_game)
        new_game_dialog.exec()

    def start_fresh_game(self, same_players: bool) -> None:
        """
        Starts a new game with the same players if requested, or prompts for a new number of players if not using the
        same players.

        :param same_players: A boolean indicating if we are using the same players from last game or not.
        :return: None
        """
        if not same_players:
            self.show_player_select()

        # set the scene
        self.scene = Scene(self.players)
        self.scene.game_over_sig.connect(self.game_over)
        view = qtw.QGraphicsView(self.scene)
        self.setCentralWidget(view)

        # End main UI code
        self.show()

    def show_player_select(self) -> None:
        """
        Presents a dialog box for choosing the number of players.

        :return: None
        """
        num_players_dialog = NumPlayersDialog()
        num_players_dialog.players.connect(self.add_players)
        num_players_dialog.exec()

    def add_players(self, players_list: list) -> None:
        """
        Adds player names to the players list.

        :param players_list: A list of the player's names.
        :return: None.
        """
        self.players = players_list


if __name__ == '__main__':
    """
    ONLY RUN THIS CODE IF THIS SCRIPT IS CALLED DIRECTLY
    """
    # QApplication represents the state of our running application
    app = qtw.QApplication(sys.argv)

    # Main Window is the 'root' widget
    mw = MainWindow()

    # Wrap app.exec in sys.exit so the exit code of the PyQt app is captured by the system
    sys.exit(app.exec())

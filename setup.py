from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='memory',
    version='1.0',
    install_requires=['pyqt6'],
    url='https://gitlab.com/ricoflow/memory',
    license='License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    author='Randall Speake',
    author_email='ranspeake@gmail.com',
    description='A memory game where you try to match pairs of hidden tiles.',
    long_description_content_type='text/markdown',
    long_description=long_description,
)
